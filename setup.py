from setuptools import setup

setup(name='basyncio',
      version='0.1.0',
      description='async with blender',
      url='https://github.com/FxIII/blender-asyncio',
      author='Fx',
      license='apache',
      packages=['basyncio'],
      install_requires=[]
      )
