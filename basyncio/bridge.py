import asyncio
import bpy

from .handlers import install_handlers
from .loopstep import generateRunSteps


class BlenderListener(object):
    def __init__(self, event_type=None, callback=None, catch=False):
        self.event_type = event_type
        self.callback = callback
        self.catch = catch
        self.event = None
        self.operator = asyncio.get_event_loop().operator
        self.operator.add_listener(self)
        self.flag = asyncio.Event()

    def check_event(self, event):
        self.event = event
        if self.event_type is not None:
            if event.type != self.event_type:
                return False, False
        if self.callback is not None:
            return self.callback(event), self.catch
        else:
            return True, self.catch

    def clear(self):
        self.flag.clear()

    @asyncio.coroutine
    def wait(self):
        yield from self.flag.wait()
        self.flag.clear()

    def remove(self):
        self.operator.remove_listener(self)


class AsyncioBridgeOperator(bpy.types.Operator):
    """Operator which runs its self from a timer"""
    bl_idname = "bpy.start_asyncio_bridge"
    bl_label = "Start Asyncio Modal Operator"

    def __init__(self):
        super().__init__()

    def __del__(self):
        pass

    def modal(self, context, event):
        if event.type == 'TIMER':
            next(self.steps)
        else:
            for listener_id, listener in self.listeners.items():
                fire, catch = listener.check_event(event)
                if fire:
                    listener.flag.set()
                    # In the case of firing an event, it is important to
                    # quit the listener processing in this loop iteration.
                    # This assures that only one asyncio.Event flag is
                    # set per iteration.
                    if catch:
                        return {'RUNNING_MODAL'}
                    else:
                        return {'PASS_THROUGH'}

        return {'PASS_THROUGH'}

    def execute(self, context):
        self.types = {}
        self.listeners = {}
        self.listener_id = 0
        self.loop = bpy.loop
        self.loop.operator = self
        self.steps = generateRunSteps(self.loop, timeout=1.0 / 100.)
        wm = context.window_manager
        wm.modal_handler_add(self)
        self._timer = wm.event_timer_add(0.005, context.window)
        return {'RUNNING_MODAL'}

    def invoke(self, context, event):
        self.execute(context)
        return {'RUNNING_MODAL'}

    def cancel(self, context):
        wm = context.window_manager
        wm.event_timer_remove(self._timer)

    def add_listener(self, listener):
        self.listeners[self.listener_id] = listener
        listener.id = self.listener_id
        self.listener_id += 1

    def remove_listener(self, listener):
        del self.listeners[listener.id]


def register():
    try:
        bpy.utils.register_class(AsyncioBridgeOperator)
    except:
        pass


def get_event_loop():
    if hasattr(bpy, "loop"):
        return bpy.loop
    register()
    loop = asyncio.get_event_loop()
    bpy.loop = loop
    if not hasattr(loop, "operator") or loop.operator is None:
        bpy.ops.bpy.start_asyncio_bridge("INVOKE_DEFAULT")
        install_handlers()
    return loop


def unregister():
    bpy.utils.unregister_class(AsyncioBridgeOperator)
